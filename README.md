# Hector CyC.
### Test for anymind


*Find latest tweets by hashtag*
[![N|Solid](https://static-assets-cyc.s3.us-east-2.amazonaws.com/images/Screen+Shot+2020-03-11+at+0.00.28.png)](https://static-assets-cyc.s3.us-east-2.amazonaws.com/images/Screen+Shot+2020-03-11+at+0.00.28.png)


*Find latest users tweets*
[![N|Solid](https://static-assets-cyc.s3.us-east-2.amazonaws.com/images/Screen+Shot+2020-03-11+at+0.00.43.png)](https://static-assets-cyc.s3.us-east-2.amazonaws.com/images/Screen+Shot+2020-03-11+at+0.00.43.png)


*Filter by fields*
[![N|Solid](https://static-assets-cyc.s3.us-east-2.amazonaws.com/images/Screen+Shot+2020-03-11+at+0.00.55.png)](https://static-assets-cyc.s3.us-east-2.amazonaws.com/images/Screen+Shot+2020-03-11+at+0.00.55.png)


*Error handling for better UX*
[![N|Solid](https://static-assets-cyc.s3.us-east-2.amazonaws.com/images/Screen+Shot+2020-03-11+at+0.00.15.png)](https://static-assets-cyc.s3.us-east-2.amazonaws.com/images/Screen+Shot+2020-03-11+at+0.00.15.png)

****
[![N|Solid](https://img.shields.io/badge/coverage-100%25-brightgreen
)]

**Dashboard to display Twitter trends serch by hashtag or user**
**NOTE** The API always return 10 tweets only.
[DEMO](https://anymind-test.firebaseapp.com) - 🔥🔥See the demo🔥🔥
  - ✅ Material Design
  - ✅ UX/UI Friendly
  - ✅ Angular 9
  - ✅ Angular  Routing
  - ✅ Angular services
  - ✅ Angular Observables
  - ✅ Firebase ready
  - ✅ Modular

# How to install

  - Clone the repository
  - NodeJS, NPM, Angular CLI Required
  - Run the following commands
    ```
    npm install
    ng serve
    ```
# How to generate documentation
Documentation is located at the "documentation" folder, 
if you want to generate new documentation 
follow the instructions below:
- Run the following commands
    ```
    npm run compodoc
    ```