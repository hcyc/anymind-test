import { Component } from "@angular/core";
import { Router } from "@angular/router";

/** Angular component metadata */
@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent {
  /** Import models and services from other libraries */
  constructor(
    /** Angular router a service that provides url navigation */
    public router: Router) {}
  
  /**
   * Redirect to dashboard on succesful login
   */
  redirectDashboard() {
    this.router.navigateByUrl("/dashboard");
  }
}
