import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FindbyuserComponent } from './findbyuser.component';

describe('FindbyuserComponent', () => {
  let component: FindbyuserComponent;
  let fixture: ComponentFixture<FindbyuserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindbyuserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindbyuserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
