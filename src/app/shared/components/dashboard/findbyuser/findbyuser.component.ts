import { Component } from "@angular/core";
import {
  NbSortDirection,
  NbSortRequest,
  NbTreeGridDataSource,
  NbTreeGridDataSourceBuilder,
  NbToastrService
} from "@nebular/theme";
import { Apiservices } from "src/app/shared/services/apiservices.service";
import * as moment from "moment";
import { TableModel } from "src/app/shared/models/tablemodel";

/** Interface to model the data format*/
interface TreeNode<T> {
  /** Data paint the rows and columns */
  data: T;
}
/** Angular component metadata */
@Component({
  selector: "app-findbyuser",
  templateUrl: "./findbyuser.component.html",
  styleUrls: ["./findbyuser.component.scss"]
})
export class FindbyuserComponent {
  /** Custom column array */
  customColumn = "text";
  /** Default columns displayed at the table */
  defaultColumns = ["likes", "replies", "retweets", "hashtags", "date"];
  /** Concatenate array of the columns to display included custom and default */
  allColumns = [this.customColumn, ...this.defaultColumns];
  /** Interface and desired model of the data */
  dataSource: NbTreeGridDataSource<TableModel>;
  /** Loader animation for better UX. Default false */
  loading = false;
  /** Variable to sort column */
  sortColumn: string;
  /** Set the direction of sort the column */
  sortDirection: NbSortDirection = NbSortDirection.NONE;
  /** Input UX status */
  inputStatus: string;

  /** Import models and services from other libraries */
  constructor(
    private dataSourceBuilder: NbTreeGridDataSourceBuilder<TableModel>,
    private webservice: Apiservices,
    private toastrService: NbToastrService
  ) {
    this.dataSource = this.dataSourceBuilder.create(this.data);
  }
  /**
   * Sort the items by column
   * @param sortRequest Receive the direction how to sort
   */
  updateSort(sortRequest: NbSortRequest): void {
    this.sortColumn = sortRequest.column;
    this.sortDirection = sortRequest.direction;
  }
  /**
   * Receive the value how to sort the column
   * @param column Sort direction to order the columnd
   */
  getSortDirection(column: string): NbSortDirection {
    if (this.sortColumn === column) {
      return this.sortDirection;
    }
    return NbSortDirection.NONE;
  }
  /**
   * Interface that shows the sample data at the
   * table
   */
  private data: TreeNode<TableModel>[] = [
    {
      data: {
        text: "Hector is hired at anymind",
        likes: 20,
        replies: 22,
        retweets: 999,
        hashtags: ["#cheers, #congrats"],
        date: moment().format("MMMM DD YYYY")
      }
    }
  ];

  /**
   * Receive the index to sort the column
   * @param index Number
   */
  getShowOn(index: number) {
    const minWithForMultipleColumns = 400;
    const nextColumnStep = 100;
    return minWithForMultipleColumns + nextColumnStep * index;
  }
  /**
   * Receive the input from the user and send it to the webservice.
   * Catch the response from the API and parse it to desired format.
   * @param event Input from the user
   */
  userChange(event: any) {
    event.target.value.trim();
    let userName = event.target.value.replace(/[^a-zA-Z0-9]/g, "");
    if (userName == "") {
      this.toastrService.danger("Special characters are not allowed", "Error");
      this.inputStatus = "danger";
      return;
    }
    this.dataSource = this.dataSourceBuilder.create([]);
    this.loading = true;
    let newData = [];
    this.webservice.getDataByUser(userName).subscribe(
      res => {
        // Validate for response errors
        if (res.error) {
          this.toastrService.danger(res.error, "Error");
          this.loading = false;
          return;
        } else if (res.count == 0) {
          this.toastrService.warning("Sorry, no results where found", "Error");
          this.loading = false;
          return;
        }
        // If the service respond successfully continue to map the object
        res.results.map(e => {
          console.log(e);
          let model = {
            data: {
              replies: e.replies,
              date: moment(e.date).format("MMMM DD YYYY"),
              hashtags: [e.hashtags[0], e.hashtags[1]],
              likes: e.likes,
              retweets: e.retweets,
              text: `${e.text.substring(0, 50)}...`
            }
          };
          newData.push(model);
        });
        this.dataSource = this.dataSourceBuilder.create(newData);
        this.loading = false;
      },
      e => {
        this.loading = false;
        this.toastrService.danger(
          "Something went wrong, please try again",
          "Error"
        );
      }
    );
  }
}
