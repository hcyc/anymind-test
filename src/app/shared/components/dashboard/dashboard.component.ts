import { Component } from '@angular/core';

/**
 * Component metadata
 */
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {

  /** Import models and services from other libraries */
  constructor() { }

}
