import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FindbyhashtagComponent } from './findbyhashtag.component';

describe('FindbyhashtagComponent', () => {
  let component: FindbyhashtagComponent;
  let fixture: ComponentFixture<FindbyhashtagComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindbyhashtagComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FindbyhashtagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
