import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import {
  NbTabsetModule,
  NbTreeGridModule,
  NbCardModule,
  NbInputModule,
  NbIconModule,
  NbSpinnerModule
} from "@nebular/theme";
import { NbEvaIconsModule } from "@nebular/eva-icons";

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NbTabsetModule,
    NbTreeGridModule,
    NbCardModule,
    NbInputModule,
    NbIconModule,
    NbEvaIconsModule,
    NbSpinnerModule
  ],
  exports: [
    NbTabsetModule,
    NbTreeGridModule,
    NbCardModule,
    NbInputModule,
    NbIconModule,
    NbEvaIconsModule,
    NbSpinnerModule
  ]
})
export class SharedModule {}
