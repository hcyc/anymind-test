/** Interface modeled for the UI*/
export interface TableModel {
  /** Contains the tweet text */
  text: string;
  /** Contains the number of likes for this tweet */
  likes: number;
  /** Contains the number of replies for this tweet */
  replies: number;
  /** Contains the number of retweets for this tweet */
  retweets: number;
  /** Contains the number of hashtags contained in this tweet */
  hashtags: Array<string>;
  /** Date when this tweet was posted */
  date: string;
}
