import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { TableModel } from "../models/tablemodel";

/**
 * API response interface, map the service response
 */
interface ApiResponse {
  /** Error if the response is bad */
  error?: object;
  /** Count of results */
  count: number;
  /** Offset parameter */
  offset: number;
  /** Results in a array */
  results: Array<TableModel>;
}
/** Injectable service */
@Injectable({
  providedIn: "root"
})
export class Apiservices {
  /** String to refer the API endpoint */
  API_ENDPOINT: string =
    "https://anymind-recruitment-python-backend.adasia.biz";

  /** Import models and services from other libraries */
  constructor(private http: HttpClient) {}

  /**
   * Service returns the latest hashtag results in the @ApiResponse format
   * @param hashtag String contains the hashtag to send the request to the server.
   */
  getDataByHashtag(hashtag: string) {
    return this.http.get<ApiResponse>(
      `https://cors-anywhere.herokuapp.com/${this.API_ENDPOINT}/hashtags/${hashtag}?offset=0`
    );
  }
  /**
   * Service returns the latest tweets by usernamein the @ApiResponse format.
   * @param username String contains the username to send the request to the server
   */
  getDataByUser(username: string) {
    return this.http.get<ApiResponse>(
      `https://cors-anywhere.herokuapp.com/${this.API_ENDPOINT}/users/${username}?offset=0`
    );
  }
}
