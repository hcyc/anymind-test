import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NbThemeModule, NbLayoutModule, NbToastrModule } from "@nebular/theme";
import { NbEvaIconsModule } from "@nebular/eva-icons";
import { LoginComponent } from "./shared/components/login/login.component";
import { DashboardComponent } from "./shared/components/dashboard/dashboard.component";
import { SharedModule } from "./shared/modules/shared.module";
import { NgxAuthFirebaseUIModule } from "ngx-auth-firebaseui";
import { environment } from "src/environments/environment";
import { MatPasswordStrengthModule } from '@angular-material-extensions/password-strength';
import { FindbyhashtagComponent } from './shared/components/dashboard/findbyhashtag/findbyhashtag.component';
import { FindbyuserComponent } from './shared/components/dashboard/findbyuser/findbyuser.component';
import { HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from './shared/components/header/header.component';

@NgModule({
  declarations: [AppComponent, LoginComponent, DashboardComponent, FindbyhashtagComponent, FindbyuserComponent, HeaderComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    NgxAuthFirebaseUIModule.forRoot(environment.firebaseConfig, undefined, {
      authGuardFallbackURL: "login",
      authGuardLoggedInURL: "dashboard"
    }),
    NbThemeModule.forRoot({ name: "default" }),
    NbToastrModule.forRoot(),
    NbLayoutModule,
    NbEvaIconsModule,
    MatPasswordStrengthModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
