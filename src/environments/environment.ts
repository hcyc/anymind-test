// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

/**
 * Firebase environment development configuration 
 */
export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBlgTEfqaSTHCE0mKM_FxF8i8oUzkiggVo",
    authDomain: "anymind-test.firebaseapp.com",
    databaseURL: "https://anymind-test.firebaseio.com",
    projectId: "anymind-test",
    storageBucket: "anymind-test.appspot.com",
    messagingSenderId: "396531803216",
    appId: "1:396531803216:web:ea6532ce6504435fc85d55"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
