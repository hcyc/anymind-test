
/**
 * Firebase environment production configuration 
 */
export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: "AIzaSyBlgTEfqaSTHCE0mKM_FxF8i8oUzkiggVo",
    authDomain: "anymind-test.firebaseapp.com",
    databaseURL: "https://anymind-test.firebaseio.com",
    projectId: "anymind-test",
    storageBucket: "anymind-test.appspot.com",
    messagingSenderId: "396531803216",
    appId: "1:396531803216:web:ea6532ce6504435fc85d55"
  }
};
